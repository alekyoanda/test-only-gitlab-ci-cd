#!/bin/sh

ssh -o StrictHostKeyChecking=no $SSH_USER@$VM_IPADDRESS << 'ENDSSH'
  cd /test-only-gitlab-ci-cd
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $CI_REGISTRY_IMAGE
  docker-compose -f docker-compose.yml up -d
ENDSSH
