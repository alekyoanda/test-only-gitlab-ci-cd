#!/bin/sh

echo DEBUG=0 >> .env
echo DEFAULT_FROM_EMAIL=blabla@gmail.com >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE >> .env