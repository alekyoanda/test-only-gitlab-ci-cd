# Use an official Python runtime as a parent image
FROM python:3.10.13-slim

# Set environment variables for Python
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# Install PostgreSQL development tools
RUN apt-get update
RUN apt-get install -y postgresql-client libpq-dev
CMD ["rm -rf /var/lib/apt/lists/*"]

# Set the working directory to /app
WORKDIR /test-only-gitlab-ci-cd

# Copy the current directory contents into the container at /app
COPY . /test-only-gitlab-ci-cd

CMD ["/bin/bash -c source venv/bin/activate"]

# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

# Expose the port that Daphne will run on
EXPOSE 1181

# Command to run your application using Daphne
# CMD ["daphne", "-u", "konekseed_be.asgi:application", "-b", "0.0.0.0", "-p", "1169"]
CMD ["python", "manage.py", "runserver", "0.0.0.0:1181"]
